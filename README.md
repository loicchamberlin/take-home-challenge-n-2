# Take Home Challenge n°2

This is the results of my research to design and to implement the solution for this assignment.
Let's summarise what was the goal. Using a Google Cloud Project and dbt Cloud, I had to : 

- **Materialise a staging table** from the raw table “transactions” that only selects the
last three months of data from this raw table

 - **Materialise a data mart table** that gives the current balance for *all addresse*s and
*exclude addresses* that had at least one transaction on Coinbase.

- From the data mart, display a graph and plot the amount of transactions on the Y
axis with the date on the X axis.

## Library needed

You will need some librairies to make the project work such as : 
```
pip install requests, google-cloud-bigquery, pandas, numpy, matplotlib.pyplot
```

## How to get the informations

By cloning the project in your IDE, you can call the API of dbt Cloud using the script [Script_DbtAPI.py](https://gitlab.com/loicchamberlin/take-home-challenge-n-2/-/blob/main/Script_DbtAPI.py) in order to start a job that creates the tables necessary in the Google Cloud Project. *(this doesn't work at the moment since I have a problem I can't resolve in the time I had)*

Then, you can visualise the data  mart table and the Table used to materialise the graph using [DataGraph.ipynb](https://gitlab.com/loicchamberlin/take-home-challenge-n-2/-/blob/main/DataGraph.ipynb).

## Conclusion

This is not a finished work and modification can be added to get a better design.
