import matplotlib.pyplot as plt
class GoogleCloudProject:
    # Class attribute
    # species = "Canis familiaris" # même attribut pour toute instance de cette classe (chose en commun)
    
    def __init__(self, projectId): # ce qu'on initialise à la création d'une instance (dépends de chaque initialisation)
        self.projectId = projectId

    # Instance methods
    def __str__(self):
        return f"Your project Id is {self.projectId}"

    # Get the DataSet Id
    def GetDataSetId(self, client):
        listDataSet = client.list_datasets(self.projectId)
        # modifier en le 1 en 0 si non présence d'Analytics
        self.dataSetId = list(listDataSet)[0].dataset_id
        print(f'Name of the DataSet used : {self.dataSetId}')
        return self.dataSetId

    # Get the different table Id
    def GetTableId(self, client):
        listTables_api = client.list_tables(self.projectId+'.'+self.dataSetId)
        self.listTables = []

        for table in listTables_api:
            self.listTables.append(table.table_id)

        return self.listTables

    # Query to get the data from the Google Cloud Project
    def PerformAQuery_forDataMart(self, client):
        QUERY = (
            f'SELECT * FROM `{self.projectId}.{self.dataSetId}.{self.listTables[1]}`' )

        ExampleofQuery = client.query(QUERY)  # API request

        DataMartTable = ExampleofQuery.result()
        DataMartTable_df = DataMartTable.to_dataframe()
        DataMartTable_df['Balance'] = DataMartTable_df['Balance'].astype(float)
        return DataMartTable_df 

    # Query to get the data from the Google Cloud Project
    def PerformAQuery_forGraph(self, client):
        QUERY = (
            f'''
            SELECT 
                DateTime, 
                sum(MoneySent) as MoneyForaDate 
            FROM 
                `{self.projectId}.{self.dataSetId}.{self.listTables[2]}`
            GROUP BY 
                DateTime''' )

        DataAndMoney_query = client.query(QUERY)  # API request

        Graph_DataFrame = DataAndMoney_query.to_dataframe()
        Graph_DataFrame['MoneyForaDate'] = Graph_DataFrame['MoneyForaDate'].astype(float)
        return Graph_DataFrame

    # Methods to show the 
    def ShowGraph_DataMart(self, df_graph, title, axis_x, axis_y):
        column_headers = list(df_graph.columns.values)

        plt.figure()
        graph = plt.plot(df_graph[column_headers[0]],df_graph[column_headers[1]])
        plt.setp(graph, color='b', linewidth=1.0)
        plt.title(title)
        plt.xlabel(axis_x)
        plt.ylabel(axis_y)
        plt.grid(True)

    
    pass