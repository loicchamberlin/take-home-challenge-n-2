with data as (
    select * from {{ ref ('Bitcoin_Cash')}}
),

data_from_three_last_months as (
    select `hash` as Addresses, DATE(block_timestamp) as DateTime, is_coinbase ,output_value as MoneySent

    from data 
    
    # We arrange the data to see first date we have
    ORDER BY block_timestamp

)

select * from data_from_three_last_months
