select * from `bigquery-public-data.crypto_bitcoin_cash.transactions` 
# Selection des trois derniers mois
WHERE DATE(block_timestamp_month) BETWEEN DATE_SUB(CURRENT_DATE(), INTERVAL 3 MONTH) AND CURRENT_DATE()
