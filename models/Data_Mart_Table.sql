with data as (
    select * from {{ ref ('Staging_Table')}}
),

data_values as (
    select Addresses, sum(MoneySent) as Balance
    from data
    group by Addresses
)

select * from data_values